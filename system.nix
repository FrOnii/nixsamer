# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  networking.networkmanager.enable = true; 

  nixpkgs.config.allowUnfree = true;

  boot.loader.systemd-boot={
    enable = true;
    configurationLimit = 5;
  };
  time.timeZone = "Europe/Paris";
  i18n.defaultLocale = "fr_FR.UTF-8";
  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Configure keymap in X11
  services.xserver.layout = "fr";
  services.xserver.xkbOptions = "eurosign:e";


  # Enable sound.
  sound.enable = true;
  
  hardware.pulseaudio.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.fronii = {
    isNormalUser = true;
    extraGroups = [ "wheel" "audio" "sound" "video" "networkmanager" "input" "tty" ]; # Enable ‘sudo’ for the user.
  };
 nix = {

    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };

 
  system = {
    autoUpgrade = {
      enable = false;
      channel = "https://nixos.org/channels/nixos-unstable";
    };
    stateVersion = "22.05";
  };

}

