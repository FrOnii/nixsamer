# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{  
  environment.systemPackages = with pkgs; [
    nano # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    picom
    git
    brave
    discord
    dunst
    networkmanager
    pavucontrol
    pulseaudioFull
    hydrus
    betterdiscordctl
    gcc
    curl
    feh
    mpv
    alacritty
    killall
    sxhkd
    bspwm
    rofi
    thunderbird
    libreoffice
    maim
    nnn
    xclip
    neofetch
    gnumake
    gzip
    python39
    python39.pkgs.pip
    unzip
    unrar-wrapper
    nodejs
    glibc
    python39.pkgs.tensorflow
    gnome.nautilus
    xournalpp
    moc
  ];
imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./system.nix
      ./jeu.nix
      ./display.nix
      <home-manager/nixos>
     ];
nixpkgs.overlays = [(self: super: { discord = super.discord.overrideAttrs (_: { src = builtins.fetchTarball https://discord.com/api/download?platform=linux&format=tar.gz; });})];
fonts.fonts = with pkgs; [
    (nerdfonts.override { fonts = [ "Cousine" ]; })
];
}

