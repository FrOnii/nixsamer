# Edit this configuration file BABD8Dto define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs,... }:
#
#
#
#
#
let
   back_bar = "3E505B";
   bar_height = 25;
   bordure = "C6CAED";
   text = "BABD8D";
   back_window = "303a45";
   bord_size = 2;
in
{
  services.xserver.enable = true;
  services.xserver.desktopManager.wallpaper.mode="fill";

  home-manager.users.fronii = {pkgs, ...}:{
    xsession.windowManager.bspwm = {
	enable = true;
	settings = {
	  border_width = bord_size;
	  windows_gape = bord_size*3;
	  single_monocle = true;
	  top_padding = bar_height;
	  split_ratio = 0.52;
	  borderless_monocle = true;
	  gapless_monocle = true;
	  focused_border_color = "#${bordure}";
	};
	monitors = { 
	  HDMI-1 = [ "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" ];
        };
	startupPrograms = [
		"systemctl --user restart polybar"
		"alacritty --class home_term"
		"alacritty --class home_music -e cmus"
		"alacritty --class home_mail"];
        extraConfig = ''
	  bspc rule -a Alacritty:home_term desktop=^1 state=floating rectangle=400x720+950+30
	  bspc rule -a Alacritty:home_music desktop=^1 state=floating rectangle=900x130+25+620
	  bspc rule -a Alacritty:home_mail desktop=^1 state=floating rectangle=900x150+25+30
	'';
	rules = {
	  "Brave-browser" = { desktop="^2"; follow=true;};
	  "discord" = { desktop="^3"; follow=true;};
          "Alacritty" = { desktop="^4"; follow=true;};
	  "Hydrus Client" = { desktop="^10"; follow=false;};
	  "mpv" = {state="fullscreen";};
	  "Pavucontrol" = {state="floating";};
	};
    };
    programs = {
      alacritty = {
        enable = true;
        settings = {
          font = rec {
	    family = "Cousine";
            bold = { style = "Bold"; };
            italic = { style = "Italic"; };
            size = 6;
          };
	  env.NNN_FIFO="/tmp/nnn.fifo";
	  #window.opacity = 1.0;
	  colors.primary.background="#${back_window}";
	  colors.normal = {
	    black = "#${back_window}"; #
	    red = "#F5916B"; #
	    green = "#9FDF80"; #
	    yellow = "#FFC89F"; #
	    blue = "#16AAE3"; #
	    magenta = "#C66D96"; #
	    cyan = "#3E9093"; #
	    white = "#${text}"; #
	  };
        };
      };
      rofi = {
        enable = true;
        extraConfig = { 
	  modi = "drun";
	};
	theme = "lb";
      };
      nnn = {
        enable = true;
        bookmarks = {
          D = "~/Downloads";
          h = "~";
	  w = "/data/";
	  c = "/data/So_Much_Files/Code/";
	  C = "/data/Many_System/Users/user/Documents/Cours/";
        };
	package = pkgs.nnn.override ({ withNerdIcons = true; });
	extraPackages = with pkgs; [ tabbed xterm nsxiv zathura ffmpegthumbnailer mediainfo sxiv ];
	plugins.src = (pkgs.fetchFromGitHub {
	  owner = "jarun";
	  repo = "nnn";
	  rev = "v4.5";
	  sha256 = "sha256-Hpc8YaJeAzJoEi7aJ6DntH2VLkoR6ToP6tPYn3llR7k=";
	}) + "/plugins";
	plugins.mappings = {
	  f = "finder";
	  p = "preview-tabbed";
	  n = "nuke";
	}; 
      };
    };
    services.sxhkd={
	enable = true;
	keybindings={
  	  # terminal
	  "super + Return"="alacritty";
	  "super + space"="rofi -show drun";
	  # reload config
	  "super + Escape"="pkill -usr1 -x sxhkd";
	  "super + shift + Escape"="bspc wm -r";
	  #Floating <->
	  "super + f"="bspc node focused -t ~floating";
	  "super + s"="bspc node -g ~sticky";
	  "super + shift + f"="bspc node focused -t ~fullscreen";
	  #change focused
	  "super + {_,shift + }{Left,Down,Up,Right}"="bspc node -{f,s} {west,south,north,east}";
	  #Change size windows
	  "super + ctrl + {Left,Down,Up,Right}"="bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}";
	  "super + alt + {Left,Down,Up,Right}"="bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}";
	  #Shortcuts
	  "super + d"="discord";
	  "super + b"="brave";
	  "super + {_, alt + ctrl } h"="hydrus-client {-d=\"/data/Very_Media/Hydrus/db\",-d=\"/data/Very_Media/Hydrus/db2\"}";
	  "super + n"="alacritty -e nnn";
	  #Change desktops
	  "super + {parenright,equal}"="bspc desktop -f {prev,next}";
	  "super + {ampersand,eacute,quotedbl,apostrophe,parenleft,minus,egrave,underscore,ccedilla,agrave}"=
	  "bspc desktop -f {1,2,3,4,5,6,7,8,9,10}";
	  #Move window to another desktop
	  "super + {1-9,0}"="bspc node -d {1-9, 10}";
	  #close/kill app
	  "super + {_,shift + }q"="bspc node -{c,k}";
	  #Screenshot
	  "super + shift + s" = "maim -s -u | xclip -selection clipboard -t image/png -i";
	  "Print" = "maim";
	};
    };
    services.polybar = {
      package = pkgs.polybar.override {
        alsaSupport = true;
        pulseSupport = true;
      };
      enable = true;
      script = "exec polybar main &";
      config = {
        "bar/main" = {
	  wm-restack = "bspwm";
	  background = "#EE${back_bar}";
	  foreground = "#FF${text}";
	  font-0="CousineNerdFontMono:size=13:weight=bold;5";
          width = "100%";
          height = bar_height;
	  padding="10px";
          modules-center = "title";
          modules-right = "date";
	  modules-left = "volume";
        };
        "module/title" = {
          type = "internal/xwindow";
	  format = "<label>";
	  label = "%title%";
        };
        "module/date" = {
          type = "internal/date";
          date = "%d/%m/%y %H:%M";
        };
      "module/volume" = {
        type = "internal/pulseaudio";
        interval = 2;
        format-volume = "<ramp-volume>  <label-volume>";
        label-muted = "";
        label-muted-foreground = "#66";
        ramp-volume-0 = "";
        ramp-volume-1 = "";
        ramp-volume-2 = "";
        click-right = "${pkgs.pavucontrol}/bin/pavucontrol";
      };

    };
  };
};
  services.xserver.displayManager = {
        defaultSession = "none+bspwm";
        lightdm = {
          enable=true;
	  background = ./wallpaper.png;
        };
 };
 services.xserver.windowManager.bspwm = {
    enable = true;
  };
  services.picom = {
    enable = true;
    vSync = true;
    activeOpacity = 1.0;
    inactiveOpacity = 0.95;
    opacityRules = [
      # always make terminals slightly transparent
      "90:class_g = 'Alacritty' && focused"
      "85:class_g = 'Alacritty' && !focused"
    ];
    fade = true;
    fadeDelta = 5;
    # this was the thing that made the tearing go away!
    backend = "glx";
  };

}
